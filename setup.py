from setuptools import setup

setup(name='dlmbot',
      version='0.1',
      description='DLM Website Bot',
      url='https://gitlab.com/mvettosi/dlmbot',
      author='Matteo Vettosi',
      author_email='matteo.vettosi@gmail.com',
      license='MIT',
      packages=['dlmbot'],
      zip_safe=False)
